import tensorflow as tf
import cv2
import numpy as np
import os
import argparse
import glob
from tqdm import tqdm
import matplotlib.pyplot as plt
import network
from tensorflow.python.util import deprecation

# depth = cv2.imread("./channel1.png")
# y = 192
# x = 24
# w = 320
# h = 195
# crop_img = depth[y:y+h, x:x+w]

# Load the TFLite model and allocate tensors.
interpreter = tf.lite.Interpreter(model_path="./frozen_models/tflite_pydnet.tflite")
interpreter.allocate_tensors()

# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
print(input_details)
print(output_details)

# Test the model on random input data.
input_shape = input_details[0]['shape']
print(input_shape)
input_data = cv2.imread("./0-reg.png")
# input_data = cv2.resize(input_data, (640, 384))
# input_data = input_data/ 255.0
input_data = np.array(input_data, dtype=np.float32)
input_data = np.expand_dims(input_data, axis=0)
print(input_data.shape)
interpreter.set_tensor(input_details[0]['index'], input_data)

interpreter.invoke()

# The function `get_tensor()` returns a copy of the tensor data.
# Use `tensor()` in order to get a pointer to the tensor.
output_data = interpreter.get_tensor(output_details[0]['index'])
test = cv2.imread("./mmymat.png")
# print(test.shape)
#test = cv2.imread("./channel1.png")
#print("TEST")
#print(test.shape)
test = np.squeeze(test)
#print(test.shape)
min_depth = test.min()
max_depth = test.max()
test = (test - min_depth) / (max_depth - min_depth)
test *= 255.0
cv2.imwrite("./testout.png", test)

print(output_data)
output_data = np.squeeze(output_data)
print(output_data.shape)
min_depth = output_data.min()
max_depth = output_data.max()
depth = (output_data - min_depth) / (max_depth - min_depth)
depth *= 255.0

cv2.imwrite("./out.png", depth)

# preparing final depth
# if opts.original_size:
#     depth = cv2.resize(depth, (w, h))
# name = os.path.basename(img_list[i]).split(".")[0]
# dest = opts.dest
# create_dir(dest)
# dest = os.path.join(dest, name + "_depth.png")
# plt.imsave(dest, depth, cmap="magma")
